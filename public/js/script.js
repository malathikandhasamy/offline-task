var data = [];
var searchText = '';
let gridwrapper = document.querySelector(".grid-wrapper");
let tableWrapper = document.querySelector(".table-wrapper");
let sortTableElement = document.querySelector(".sort-table");
let searchBox = document.querySelector(".search-box");
let loader = document.createElement('div')
let tableSort = 'none'
let tableSortOption = ['asc', 'desc', 'none']

loader.className = 'loader-wrapper'
loader.innerHTML = `<div class="loader"></div>
<div class="loader-text">Fetching</div>`;

function showLoader() {
    document.body.prepend(loader)
}
function hideLoader() {
    loader.remove()
}


function main() {
    var currentApi = 1;
    var gridView = true;

    showLoader();
    fetch('/api/page1')
        .then(res => res.json())
        .then(result => {
            hideLoader()
            data = [
                ...result
            ]
            displayCard(data);
            let x = document.querySelector('input').value;
            console.log(x);
        })

    let tabItems = document.querySelectorAll('.tabs .tab-item')
    for (const tabItem of tabItems) {
        tabItem.addEventListener('click', function (e) {
            let mode = e.target.getAttribute('data-mode')
            for (const tabItem of tabItems) {
                tabItem.className = "tab-item";
            }
            e.target.className += ' active'
            gridView = mode === 'grid';
            displayCard(data);
        })
    }

    let displayCard = (result) => {
        if (searchText) {
            let newData = data.filter(f => f.name.toLowerCase().includes(searchText))
            result = newData
        }
        if (gridView) {
            tableWrapper.className += ' hide'
            gridwrapper.className = gridwrapper.className.replace(/ hide/g, '')
            gridwrapper.innerHTML = "";
            // result.slice(result.length - 80, result.length).map((data, inx) => {
            result.map((data, inx) => {
                gridwrapper.innerHTML += `
                    <div class="item">
                    <div class="card w-100">
                    <div class="image" style="background-image:url(${data.image})"> </div>
                        <div class="card-body">
                            <h2 class="title">
                               ${data.name}
                            </h2>
                            <p class="description">${data.description}</p>
                        </div>
                    </div>
                    </div>
                    `
            })
        } else {
            if (tableSort != 'none') {
                result = result.sort((a, b) => {
                    if (a.name < b.name) {
                        return tableSort == 'asc' ? -1 : 1;
                    }
                    if (a.name > b.name) {
                        return tableSort == 'asc' ? 1 : -1;
                    }
                    return 0;
                })
            }
            gridwrapper.className += " hide";
            tableWrapper.className = tableWrapper.className.replace(/ hide/g, '')
            let tableBodyWrapper = document.querySelector(".table-wrapper tbody");
            tableBodyWrapper.innerHTML = "";
            // result.slice(result.length - 80, result.length).map(data => {
            result.map(data => {
                tableBodyWrapper.innerHTML += `
                            <tr>
                                <td>
                                    <img src="${data.image}" height="100" />
                                    
                                </td>
                                <td class="data-center">
                                    <h2>${data.name}</h2>
                                </td>
                                <td>
                                    <p>${data.description}</p>
                                </td>
                            </tr>

                `
            })
        }
    }

    window.onscroll = function (ev) {
        var scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        var scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body
            .scrollHeight;
        var scrolledToBottom = (scrollTop + window.innerHeight) >= scrollHeight-5;

        if (scrolledToBottom) {
            currentApi++;
            if (currentApi == 4) {
                currentApi = 1;
            }
            showLoader();
            fetch('/api/page' + currentApi)
                .then(res => res.json())
                .then(result => {
                    hideLoader()
                    data = [
                        ...data,
                        ...result
                    ]
                    displayCard(data);

                })
        }
    }

    searchBox.addEventListener('input', function (e) {
        searchText = e.target.value.toLowerCase()
        displayCard(data)
    })

    sortTableElement.addEventListener('click', function (e) {
        let inx = tableSortOption.indexOf(tableSort)

        if (inx === 2) {
            tableSort = tableSortOption[0]
        }
        else {
            tableSort = tableSortOption[inx + 1]
        }
        displayCard([...data])
    })

}

main()