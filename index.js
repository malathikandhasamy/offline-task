const express = require('express')
const app = express()
const port = 8080
const axios = require('axios');
app.use(express.static('public'));

app.get('/api/page1', (req, res) => {
  axios('http://run.mocky.io/v3/6f7a76ed-d6f5-4b54-be23-bf9a141c982a')
    .then(data => {
      res.send(data.data);
    })
    .catch(err => console.log(err))
})
app.get('/api/page2', (req, res) => {
  axios('http://run.mocky.io/v3/07316365-b8d2-4574-9bc1-22b17b054e3b')
    .then(data => {
      res.send(data.data);
    })
    .catch(err => console.log(err))
})
app.get('/api/page3', (req, res) => {
  axios('http://run.mocky.io/v3/1c56213e-1191-4b47-a54f-066736165ff3')
    .then(data => {
      res.send(data.data);
    })
    .catch(err => console.log(err))
})
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})